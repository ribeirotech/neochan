import { Component, OnInit, Input } from '@angular/core';
import { Post } from "../post/post";
import { Thread } from './thread';

@Component({
  selector: 'app-thread',
  templateUrl: './thread.component.html',
  styleUrls: ['./thread.component.scss']
})
export class ThreadComponent implements OnInit {

  posts: Post[];
  @Input() thread: Thread;
  @Input() isDetail: Boolean;

  constructor() { }

  ngOnInit() {
    
  }

}
