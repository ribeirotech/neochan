import { Component, OnInit, Input } from '@angular/core';
import { Thread } from '../thread';
import { ThreadService } from '../thread.service';
import { BoardService } from 'src/app/board/board.service';

@Component({
  selector: 'app-new-thread',
  templateUrl: './new-thread.component.html',
  styleUrls: ['./new-thread.component.scss']
})
export class NewThreadComponent implements OnInit {

  submitted: boolean;
  thread = new Thread();
  boards: Object;

  constructor(private threadService: ThreadService, private boardService: BoardService) { }

  ngOnInit() {
    this.submitted = false;
    this.getBoards();
  }

  onSubmit() {
    if (!this.submitted) {
      this.submitted = true;

      this.threadService.postThread(this.thread).subscribe();
      
    }
  }
  
  getBoards() {
    this.boardService.getBoards().subscribe( (data) => { this.boards = data });
  }

}
