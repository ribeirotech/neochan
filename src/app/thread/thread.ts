import {Post} from "../post/post";

export class Thread {
    id: number;
    posts: Post[];
}
