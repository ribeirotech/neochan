import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ThreadService {

  api = environment.api;

  constructor(private http: HttpClient) { }

  getThreads(board_name) {
    return this.http.get(this.api + "boards/" + board_name);
  }

  getThread(thread_id) {
    return this.http.get(this.api + "threads/" + thread_id);
  }

  postThread(form) {
    return this.http.post(this.api + "threads/", form);
  }
}
