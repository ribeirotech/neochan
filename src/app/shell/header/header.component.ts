import { Component, OnInit } from '@angular/core';
import { BoardService } from '../../board/board.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  boards: any;
  board_name: string;
  newThreadLink: string;

  constructor(private boardService: BoardService, private route: ActivatedRoute, private router: Router) {
    router.events.subscribe( (event) => {
      if (event instanceof NavigationEnd) {
       
        this.board_name = this.route.snapshot.paramMap.get('board_name');

      }
    });
   }

  ngOnInit() {
    this.getBoards();
  }

  getBoards() {
    this.boardService.getBoards().subscribe( (data) => { this.boards = data });
  }

}
