import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule }   from '@angular/forms';

import { BoardComponent } from './board/board.component';
import { ThreadComponent } from './thread/thread.component';
import { PostComponent } from './post/post.component';
import { HeaderComponent } from './shell/header/header.component';
import { FooterComponent } from './shell/footer/footer.component';
import { NewThreadComponent } from './thread/new-thread/new-thread.component';
import { NewPostComponent } from './post/new-post/new-post.component';


@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    ThreadComponent,
    PostComponent,
    HeaderComponent,
    FooterComponent,
    NewThreadComponent,
    NewPostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
