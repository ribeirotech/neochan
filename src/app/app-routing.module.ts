import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BoardComponent } from './board/board.component';
import { NewThreadComponent } from './thread/new-thread/new-thread.component';
import { NewPostComponent } from './post/new-post/new-post.component';

const routes: Routes = [
  {
    path: 'new-thread',
    component: NewThreadComponent
  },
  {
    path: 'new-post/:thread_id',
    component: NewPostComponent
  },
  {
    path: 'threads/:thread_id',
    component: BoardComponent
  },
  {
    path: ':board_name',
    component: BoardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
