import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  api = environment.api;

  constructor(private http: HttpClient) { }

  postPost(post: Object) {
    return this.http.post(this.api + 'posts', post);
  }
}
