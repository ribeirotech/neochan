import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../post';
import { PostService } from '../post.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {

  submitted: boolean;
  post = new Post;

  constructor(private postService: PostService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.submitted = false;
  }

  onSubmit() {
    if (!this.submitted) {
      this.submitted = true;
      this.post['thread_id'] = this.route.snapshot.paramMap.get('thread_id');
      this.postService.postPost(this.post).subscribe();
    }
  }

}
