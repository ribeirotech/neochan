export class Post {
    id: number;
    threadId: number;
    image: string;
    title: string;
    text: string;
  }