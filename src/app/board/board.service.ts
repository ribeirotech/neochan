import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BoardService {

  api:string = environment.api;

  constructor(private http: HttpClient) { }

  getBoards() {
    return this.http.get(this.api + "boards");
  }
}
