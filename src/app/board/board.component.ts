import { Component, OnInit, Input } from '@angular/core';
import { BoardService } from './board.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { ThreadService } from '../thread/thread.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

  threads: any;
  board_name: any;
  thread_id: any;
  isDetail: any;

  constructor(private boardService: BoardService, private threadService: ThreadService, private route: ActivatedRoute, private router: Router) {
    router.events.subscribe( (event) => {
      if (event instanceof NavigationEnd) {
        this.thread_id = this.route.snapshot.paramMap.get('thread_id');
        if (this.thread_id) {

          this.isDetail = true;
          this.getThread(this.thread_id);

        } else {

          this.board_name = this.route.snapshot.paramMap.get('board_name');
          this.getThreads();

        }
        
      }
    });
   }

  ngOnInit() {
    
  }
  
  getThread(thread_id){
    this.threadService.getThread(thread_id).subscribe( (data) => { this.threads = data });
  }

  getThreads(){
    this.threadService.getThreads(this.board_name).subscribe( (data) => { this.threads = data });
  }

}
